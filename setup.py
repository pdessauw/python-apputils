from setuptools import setup, find_packages

setup(
    name="apputils",
    version="0.1.0-a2",
    author="Philippe Dessauw",
    author_email="pdessauw@gmail.com",

    packages=find_packages('src'),
    package_dir={'': 'src'},

    install_requires=[
        'pyyaml',
    ],
)