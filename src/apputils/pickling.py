"""

"""
import cPickle
import pickle


def save(obj, filename):
    """

    :param obj:
    :param filename:
    :return:
    """
    with open(filename, "wb") as pickle_file:
        cPickle.dump(obj, pickle_file, pickle.HIGHEST_PROTOCOL)


def load(filename):
    """

    :param filename:
    :return:
    """
    with open(filename, "rb") as pickle_file:
        obj = cPickle.load(pickle_file)

    return obj