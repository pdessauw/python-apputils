from zipfile import ZipFile
from os import walk, remove
from os.path import join, splitext
from shutil import rmtree
from hashlib import sha256

module_conf = {
    "zip_ext": ".zip"
}


def zip_directory(directory):
    """

    :param directory:
    :return:
    """
    archive_name = directory + module_conf["zip_ext"]
    zip_dir = ZipFile(archive_name, "w")

    for root, folders, files in walk(directory):
        for item in folders+files:
            orig_path = join(root, item)
            dest_path = orig_path[len(directory):]

            zip_dir.write(orig_path, dest_path)

    rmtree(directory)  # Clean directory
    return archive_name


def unzip_directory(archive):
    """

    :param archive:
    :return:
    """
    zip_dir = ZipFile(archive, "r")
    directory = splitext(archive)[0]

    zip_dir.extractall(directory)
    remove(archive)

    return directory


def file_checksum(filename):
    """

    :param filename:
    :return:
    """
    return sha256(open(filename).read()).hexdigest()

