"""
"""
import logging.config
from os import makedirs
from os.path import exists, join, isfile, splitext
import yaml

app_config = None


def load_config(filename):
    """Load a YAML configuration file. All data is stored in :attr:`denoiser.app_config`.

    Parameters:
        filename (:func:`str`): YAML configuration file
    """
    # TODO Check any yaml malformed document
    global app_config

    with open(filename, "r") as conf_file:
        app_config = yaml.load(conf_file)

    install_dir = app_config["root"]
    # TODO Remove following line
    # install_app()  # Installs necessary folders if they are not there yet (to avoid errors)

    # Logging configuration
    if "log_conf" in app_config.keys():
        log_conf_name = join(install_dir, app_config["log_conf"])
        with open(log_conf_name, "r") as log_conf_file:
            log_conf = yaml.load(log_conf_file)

            # Append the log folder to the log filename
            for handler in log_conf["handlers"].values():
                if "filename" in handler.keys():
                    # handler["filename"] = join(app_config["files"]["log_dir"], handler["filename"])
                    handler["filename"] = join(install_dir, app_config["dirs"]["logs"], handler["filename"])

            logging.config.dictConfig(log_conf)

        del app_config["log_conf"]  # Information is no longer needed

    # Import other YAML configuration file
    # FIXME Recursively check for YAML files(?)
    for key, value in app_config.items():
        # if type(value) == str and isfile(join(value)) and splitext(value)[1] == ".yaml":
        if type(value) == str and isfile(join(install_dir, value)) and splitext(value)[1] == ".yaml":
            with open(join(install_dir, value), "r") as subconf_file:
                app_config[key] = yaml.load(subconf_file)


def install_app():
    """Reads the :attr:`denoiser.app_config` attribute to check that every required object has been created. If not this
    function creates it.

    Returns:
        (int) return code (0 if everything is good)
    """
    # FIXME Currently not adapted for the main configuration file
    global app_config

    if app_config is None or "files" not in app_config or type(app_config["files"]) != list:
        return -1

    for directory in app_config["files"].values():
        try:
            if not exists(directory):
                makedirs(directory)
        except:
            return -2

    return 0


def get_config(key):
    if app_config is None:
        raise ValueError("App config not loaded")

    try:
        if '/' in key:
            keys = key.split('/')

            tmp_keys = []
            for k in keys:
                sp = k.split('#')

                if len(sp) != 1:
                    sp[1] = int(sp[1])

                tmp_keys += sp

            keys = tmp_keys
            config_data = app_config
            for k in keys:
                config_data = config_data[k]

            return config_data
        else:
            return app_config[key]
    except:
        raise KeyError("Key "+str(key)+" not present in config file")
