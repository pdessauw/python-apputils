# python-apputils

This Python package contains some useful functions that can be easily reused inside every Python project. It covers:

* Configuring your app using Yaml configuration files,
* Zipping and unizipping directories on the fly.

## Installation

### Packaging source files

	> git clone https://bitbucket.org/pdessauw/python-apputils.git
	> git checkout master
	> git pull
	> cd python-apputils
	> python setup.py sdist

You should now see that a **dist** package appeared in the main directory.

### Installing the package

	> cd path/to/python-apputils/dist
	> pip install apputils-*version*.tar.gz

You can now use this package to its full extent!

## Using the package

*This part has not yet been written.*

## Contribution guidelines

* Writing tests
* Code review
* Other guidelines

## Who do I talk to?

* Repo owner or admin
* Other community or team contact